﻿using System;
using System.Collections;
using System.Collections.Generic;
using TutorialProject.Scripts;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    [SerializeField, Tooltip("Я всплывающая подсказка")] private GameObject _ballPrefab;

    [SerializeField] private Vector3 _initialForce;

    [SerializeField] private Transform _parent;

    [SerializeField] private Vector3 _initialPosition;
    [SerializeField] private Vector3 _fieldSize;
    
    public void Spawn()
    {
        var newBallPostion = _initialPosition;
        
        var newBall = Instantiate(_ballPrefab, newBallPostion, Quaternion.identity, _parent);

        var newBallBody = newBall.GetComponent<Rigidbody>();
        
        newBallBody.AddForce(_initialForce);
    }
}
