﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Gun : MonoBehaviour
{
    [SerializeField] private float _distance = 100;
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private float _initialForce;

    private Vector3 _inputVector;

    private float _realDistance;

    private void Start()
    {
        PlayerInput.OnInput += RememberInput;
    }

    private void RememberInput(Vector3 inputMoveDirection)
    {
        inputMoveDirection.Normalize();
        
        if (inputMoveDirection == Vector3.zero) return;
        
        _inputVector = inputMoveDirection;

        var r = new System.Random();
        
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Action<RaycastHit> onShot = 
                (h) =>
                {
                    _realDistance = h.distance;

                    if (h.transform.gameObject.tag != "Ball") return;
                    
                    Destroy(h.transform.gameObject);
                };

            RaycastHit hit;
            if (Physics.Raycast(transform.position, _inputVector, out hit, _distance)) onShot.Invoke(hit);
            if (Physics.Raycast(transform.position, _inputVector + Vector3.down, out hit, _distance)) onShot.Invoke(hit);
            if (Physics.Raycast(transform.position, _inputVector + Vector3.up, out hit, _distance)) onShot.Invoke(hit);
        }
            
        Debug.DrawRay(transform.position, _inputVector * _realDistance, Color.red);
        Debug.DrawRay(transform.position, (_inputVector + Vector3.down) * _realDistance, Color.red);
        Debug.DrawRay(transform.position, (_inputVector + Vector3.up) * _realDistance, Color.red);
    }
}
