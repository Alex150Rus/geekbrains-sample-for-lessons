﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _lifeTimeAfterCollision = 2;
    [SerializeField] private float _lifeTimeInSec = 5;

    private bool _isAlreadyCollided = false;
    
    private void Start()
    {
        Invoke(nameof(DestroySelf), _lifeTimeInSec);
    }

    private void DestroySelf()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (_isAlreadyCollided) return;
        
        CancelInvoke(nameof(DestroySelf));
        
        _isAlreadyCollided = true;
        Destroy(gameObject, _lifeTimeAfterCollision);
    }
}
